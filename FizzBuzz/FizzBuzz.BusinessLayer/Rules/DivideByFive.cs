﻿using FizzBuzz.BusinessLayer.Interface;
using System;

namespace FizzBuzz.BusinessLayer.Rules
{
    public class DivideByFive : IDivide
    {
        private readonly IDayChecker dayChecker;

        public DivideByFive(IDayChecker dayChecker)
        {
            this.dayChecker = dayChecker;
        }
        public bool IsDivisible(int Value)
        {
            return Value % 5 == 0;
        }
        public string FizzBuzzLiteral()
        {
            return dayChecker.IsWednesday(DateTime.Now.DayOfWeek.ToString()) ? Constants.Wuzz : Constants.Buzz;
        }
    }
}