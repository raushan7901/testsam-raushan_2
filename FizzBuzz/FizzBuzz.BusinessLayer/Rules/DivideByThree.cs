﻿using FizzBuzz.BusinessLayer.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FizzBuzz.BusinessLayer.Rules
{
    public class DivideByThree : IDivide
    {
        private readonly IDayChecker dayChecker;

        public DivideByThree(IDayChecker dayChecker)
        {
            this.dayChecker = dayChecker;
        }
        public bool IsDivisible(int Value)
        {
            return Value % 3 == 0;
        }
        public string FizzBuzzLiteral()
        {
            return dayChecker.IsWednesday(DateTime.Now.DayOfWeek.ToString()) ? Constants.Wizz : Constants.Fizz;
        }
    }
}