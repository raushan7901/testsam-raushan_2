﻿using System.Collections.Generic;

namespace FizzBuzz.BusinessLayer.Interface
{

    public interface IFizzBuzzService
    {
        IEnumerable<string> FizzBuzzList(int number);
    }
}
