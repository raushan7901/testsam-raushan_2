﻿namespace FizzBuzz.BusinessLayer
{
    public class Constants
    {
        public const string Fizz = "FIZZ";
        public const string Buzz = "BUZZ";
        public const string SpecifiedDay = "Thursday";
        public const string Wizz = "Wizz";
        public const string Wuzz = "Wuzz";

    }
}