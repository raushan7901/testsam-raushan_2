﻿using FizzBuzz.BusinessLayer.Interface;
using FizzBuzz.Web.Models;
using PagedList;
using System.Web.Mvc;

namespace FizzBuzz.Web.Controllers
{
    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzService FizzBuzzService;
        private const int pageSize = 10;
        public FizzBuzzController(IFizzBuzzService FizzBuzzservice)
        {
            this.FizzBuzzService = FizzBuzzservice;
        }
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult FizzBuzz(FizzBuzzNumberModel fizzBuzzModel)
        {
            if (ModelState.IsValid)
            {
                fizzBuzzModel.FizzBuzzList = this.GetFizzBuzzPagedList(fizzBuzzModel.Number, 1);
            }
            return View("Index", fizzBuzzModel);
        }
        private IPagedList<string> GetFizzBuzzPagedList(int inputNumber, int pageNumber)
        {
            var fizzBuzzList = FizzBuzzService.FizzBuzzList(inputNumber);
            return fizzBuzzList.ToPagedList(pageNumber, pageSize);
        }
        [HttpGet]
        public ActionResult GetPagedList(FizzBuzzNumberModel fizzBuzzModel, int page)
        {
            fizzBuzzModel.FizzBuzzList = this.GetFizzBuzzPagedList(fizzBuzzModel.Number, page);
            return View("Index", fizzBuzzModel);
        }
    }
}