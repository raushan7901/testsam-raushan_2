﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FizzBuzz.Web.Models
{
    public class FizzBuzzNumberModel
    {
        [Display(Name = "Enter a number")]
        [Required(ErrorMessage = "*Enter the positive number")]
        [Range(1, 1000, ErrorMessage = "Enter number between 1 to 1000")]
        public int Number { get; set; }
        public IPagedList<string> FizzBuzzList { get; set; }
    }
}